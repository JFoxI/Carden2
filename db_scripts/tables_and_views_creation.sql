USE postcard_db;

CREATE TABLE db_user (user_id INT UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY, user_name VARCHAR(20) NOT NULL UNIQUE, password_hash CHAR(128) NOT NULL); /*hash plaanime teha SHA-512*/

CREATE TABLE postcard (card_id INT UNSIGNED NOT NULL PRIMARY KEY, reviewed Boolean DEFAULT 0);/* reviewed alustab default false st. kaart vajab veel ülevaatamist (reviewed)*/

CREATE TABLE card_transcript (transcript_id INT UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY, transcript_text VARCHAR(1000),user_name VARCHAR(20) NOT NULL,
 transcript_datetime DATETIME, card_id INT UNSIGNED NOT NULL);
 
CREATE TABLE card_comment (comment_id INT UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY, comment_text VARCHAR(1000),user_name VARCHAR(20) NOT NULL,
 comment_datetime DATETIME, card_id INT UNSIGNED NOT NULL);

 
ALTER TABLE card_transcript ADD FOREIGN KEY (user_name) REFERENCES db_user(user_name);
ALTER TABLE card_transcript ADD FOREIGN KEY (card_id) REFERENCES postcard(card_id);
ALTER TABLE card_transcript
CHANGE COLUMN transcript_datetime transcript_datetime DATETIME NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE card_comment ADD FOREIGN KEY (user_name) REFERENCES db_user(user_name);
ALTER TABLE card_comment ADD FOREIGN KEY (card_id) REFERENCES postcard(card_id);
ALTER TABLE card_comment
CHANGE COLUMN comment_datetime comment_datetime DATETIME NULL DEFAULT CURRENT_TIMESTAMP;

				 
CREATE VIEW unreviewed  AS SELECT * FROM postcard WHERE reviewed=0;

CREATE VIEW reviewed  AS SELECT * FROM postcard WHERE reviewed=1;


CREATE VIEW transcript_view AS
    SELECT 
		card_transcript.transcript_id AS transcript_id,
        card_transcript.transcript_text AS transcript_text,
        card_transcript.user_name AS user_name,
        UNIX_TIMESTAMP(card_transcript.transcript_datetime) AS transcript_datetime,
        card_transcript.card_id AS card_id
       
    FROM
        card_transcript;
		



CREATE VIEW comment_view AS
    SELECT 
		card_comment.comment_id AS comment_id,
        card_comment.comment_text AS comment_text,
        card_comment.user_name AS user_name,
        UNIX_TIMESTAMP(card_comment.comment_datetime) AS comment_datetime,
        card_comment.card_id AS card_id
       
    FROM
        card_comment;		
		
  

