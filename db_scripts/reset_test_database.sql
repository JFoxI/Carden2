USE postcard_db;
TRUNCATE card_comment;
TRUNCATE card_transcript;
UPDATE postcard SET reviewed=0;
/* for the last command to work in MysqlWorkbench
got Edit->Preferences->SQLEditor->remove a tick from Safe Updates box*/