package ee.bcs.koolitus.postcards.controller;


import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import ee.bcs.koolitus.postcards.resources.PostcardResource;

@Path("/postcards")
public class PostcardController {

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public int getFirstUnreviewedPostcardID() {
		return PostcardResource.getFirstUnreviewedPostcardID();
	}
	
	@GET
	@Path("/{card_id}")
	@Produces(MediaType.TEXT_PLAIN)
	public int getNextUnreviewedPostcardID(@PathParam("card_id") int currentCard_id) {
		return PostcardResource.getNextUnreviewedPostcardID(currentCard_id);
	}
	
	@PUT
	@Path("/{card_id}")
	@Produces(MediaType.TEXT_PLAIN)
	public void changeReviewedStatus(@PathParam("card_id") int currentCard_id) {
		PostcardResource.changeReviewedStatus(currentCard_id);
	}

	@GET
	@Path("/reviewed")
	@Produces(MediaType.TEXT_PLAIN)
	public int getFirstReviewedPostcardID() {
		return PostcardResource.getFirstReviewedPostcardID();
	}

	
	@GET
	@Path("/reviewed/{card_id}")
	@Produces(MediaType.TEXT_PLAIN)
	public int getNextReviewedPostcardID(@PathParam("card_id") int currentCard_id) {
		return PostcardResource.getNextReviewedPostcardID(currentCard_id);
	}
}
