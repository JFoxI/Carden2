package ee.bcs.koolitus.postcards.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcs.koolitus.postcards.dao.Comment;
import ee.bcs.koolitus.postcards.resources.CommentResource;

@Path("/comments")
public class CommentController {
	
	@GET
	@Path("/{card_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Comment> getAllComments(@PathParam("card_id") int currentCard_id) {
		return CommentResource.getAllCommentsByCardId(currentCard_id);
	}
	
		
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public void addComment(Comment comment) {
		CommentResource.addNewComment(comment);
	}	
}
