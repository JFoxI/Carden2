package ee.bcs.koolitus.postcards.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcs.koolitus.postcards.dao.User;
import ee.bcs.koolitus.postcards.resources.UserResource;

@Path("/users")
public class UserController {
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)	
	public boolean checkUser(User user) {
		return UserResource.isUserInDatabase(user);
	}
	
	
	@POST
	@Path("/newUser")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)	
	public boolean createNewUser(User user) {
		return UserResource.createNewUser(user);
	}
	
	@POST
	@Path("/checkUser")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)	
	public boolean checkUsername(User user) {
		return UserResource.isUsernameInDatabase(user);
	}
}
