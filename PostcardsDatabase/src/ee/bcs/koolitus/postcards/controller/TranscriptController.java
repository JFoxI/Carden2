package ee.bcs.koolitus.postcards.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcs.koolitus.postcards.dao.Transcript;
import ee.bcs.koolitus.postcards.resources.TranscriptResource;

@Path("/transcripts")
public class TranscriptController {
	
	@GET
	@Path("/{card_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Transcript> getAllTranscripts(@PathParam("card_id") int currentCard_id) {
		return TranscriptResource.getAllCardTranscriptsByCardId(currentCard_id);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public void addTranscript(Transcript transcript) {
		TranscriptResource.addNewTranscript(transcript);
	}

}
