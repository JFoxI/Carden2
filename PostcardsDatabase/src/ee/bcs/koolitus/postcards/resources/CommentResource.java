package ee.bcs.koolitus.postcards.resources;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import ee.bcs.koolitus.postcards.dao.Comment;

public abstract class CommentResource {

	public final static Logger log = Logger.getLogger(CommentResource.class);
	
	public static List<Comment> getAllCommentsByCardId(int currentCard_id) {
		
		log.info("User looked all comments");
		
		List<Comment> allCardComments = new ArrayList<>();
		
		String sqlQuery = "SELECT * FROM comment_view WHERE card_id=? ;";
		
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery);) {
			statement.setInt(1, currentCard_id);
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				Comment cardComment = new Comment();
				cardComment.setCardId(results.getInt("card_id"));
				cardComment.setCommentId(results.getInt("comment_id"));
				cardComment.setCommentText(results.getString("comment_text"));
				cardComment.setDatetime(new Date(results.getLong("comment_datetime")*1000)); //second to millisecond
				cardComment.setUserName(results.getString("user_name"));
				allCardComments.add(cardComment);
			}
		} catch (SQLException e) {
			log.error("Error on getting all card comments" + e);
		}
		
		Collections.sort(allCardComments);
		return allCardComments;
	}

	
	public static void addNewComment(Comment comment) {
		
		String sqlQuery = "INSERT INTO card_comment (comment_text, card_id, user_name) VALUES (?, ?, ?);";

		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setString(1, comment.getCommentText());
			statement.setInt(2, comment.getCardId());
			statement.setString(3, comment.getUserName());
			statement.executeUpdate();
		}
		catch (SQLException e) {
			log.error("Error adding new comment" + e);
		}
	}
}
