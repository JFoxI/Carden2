package ee.bcs.koolitus.postcards.resources;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

public abstract class PostcardResource {

	public final static Logger log = Logger.getLogger(CommentResource.class);
	
	/* TODO think of exceptions mainly when we reach the last postcard */
	public static int getFirstUnreviewedPostcardID() {
		
		String sqlQuery = "SELECT card_id FROM unreviewed LIMIT 1 ;";

		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {
			while (results.next()) {
				return results.getInt("card_id");
			}
		} catch (Exception e) {
			log.error("Problem getting first unreviewed postcards" + e);
			return 0;
		}

		return 0;
	}

	
	/* TODO think of exceptions mainly when we reach the last postcard */
	public static int getNextUnreviewedPostcardID(int currentPostcardId) {

		String sqlQuery = "SELECT card_id FROM unreviewed WHERE card_id=(SELECT min(card_id) FROM unreviewed WHERE card_id > ?);";

		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery);) {
			statement.setInt(1, currentPostcardId);
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				return results.getInt("card_id");
			}
		} catch (Exception e) {
			log.error("Problem getting next postcards" + e);
			return 0;
		}

		return 0;
	}


	public static int getFirstReviewedPostcardID() {

		String sqlQuery = "SELECT card_id FROM reviewed LIMIT 1 ;";

		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {
			while (results.next()) {
				return results.getInt("card_id");
			}
		} catch (Exception e) {
			log.error("Problem getting first reviewed postcards" + e);
			return 0;
		}

		return 0;

	}
	
	
	public static int getNextReviewedPostcardID(int currentPostcardId) {

		String sqlQuery = "SELECT card_id FROM reviewed WHERE card_id=(SELECT min(card_id) FROM reviewed WHERE card_id > ?);";

		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery);) {
			statement.setInt(1, currentPostcardId);
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				return results.getInt("card_id");
			}
		} catch (Exception e) {
			log.error("Problem getting next postcards" + e);
			return 0;
		}

		return 0;
	}


	public static void changeReviewedStatus(int currentPostcardId) {

		String sqlQuery = "UPDATE postcard SET reviewed=1 WHERE card_id=?;";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery);) {
			statement.setInt(1, currentPostcardId);
			statement.executeUpdate();			
		} catch (Exception e) {
			log.error("Problem getting next postcards" + e);
		}
	}
}
