package ee.bcs.koolitus.postcards.resources;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import ee.bcs.koolitus.postcards.dao.Transcript;

public abstract class TranscriptResource {
	
	public final static Logger log = Logger.getLogger(CommentResource.class);
	
	public static List<Transcript> getAllCardTranscriptsByCardId(int currentCard_id) {
		
		List<Transcript> allCardTranscripts = new ArrayList<>();
		
		String sqlQuery = "SELECT * FROM transcript_view WHERE card_id = ?;";
		
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery);) {
			statement.setInt(1, currentCard_id);
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				Transcript cardTranscript = new Transcript();
				cardTranscript.setCardId(results.getInt("card_id"));
				cardTranscript.setTranscriptId(results.getInt("transcript_id"));
				cardTranscript.setTranscriptText(results.getString("transcript_text"));
				cardTranscript.setDatetime(new Date(results.getLong("transcript_datetime") * 1000));// second to millisecond
				cardTranscript.setUserName(results.getString("user_name"));
				allCardTranscripts.add(cardTranscript);
			}
		} catch (SQLException e) {
			log.error("Error on getting all card transcripts" + e);
		}
		Collections.sort(allCardTranscripts);
		return allCardTranscripts;
	}

	
	public static void addNewTranscript(Transcript transcript) {
		
		String sqlQuery = "INSERT INTO card_transcript (transcript_text, card_id, user_name) VALUES (?,?,?);";

		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setString(1, transcript.getTranscriptText());
			statement.setInt(2, transcript.getCardId());
			//TODO need user_name
			statement.setString(3, transcript.getUserName());
			statement.executeUpdate();	
		} catch (SQLException e) {
			log.error("Error adding new transcript" + e);
		}
	}
}
