package ee.bcs.koolitus.postcards.resources;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import ee.bcs.koolitus.postcards.dao.User;


public abstract class UserResource {
	
	public final static Logger log = Logger.getLogger(CommentResource.class);
	
	public static boolean isUserInDatabase(User user) {

		boolean response = false;

		String sqlQuery = "SELECT * FROM postcard_db.db_user WHERE user_name=? AND password_hash=?;";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setString(1, user.getUsername());
			statement.setString(2, user.getPwHash());
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				response = true;
			}
		} catch (SQLException e) {
			log.error("Error checking user in database " + e);
		}
		
		return response;
	}
	
	public static boolean isUsernameInDatabase(User user) {

		boolean response = false;

		String sqlQuery = "SELECT * FROM postcard_db.db_user WHERE user_name=?;";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setString(1, user.getUsername());
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				response = true;
			}
		} catch (SQLException e) {
			log.error("Error checking user in database " + e);
		}
		
		return response;
	}
	
	
	public static boolean createNewUser(User user) {

		boolean isSuccessful = false;

		String sqlQuery = "INSERT INTO db_user (user_name, password_hash) VALUES (?, ?);";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery);) {
			statement.setString(1, user.getUsername()); 
			statement.setString(2, user.getPwHash());
			Integer resultCode = statement.executeUpdate();
			if (resultCode==1) {
				isSuccessful = true;
			}
		} catch (SQLException e) {
			log.error("Error checking user in database " + e);
		}

		return isSuccessful;

	}

}

