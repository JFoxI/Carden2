package ee.bcs.koolitus.postcards.resources;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;

public class DatabaseConnection {
	
	public final static Logger log = Logger.getLogger(DatabaseConnection.class);
	
	private static Connection connection = null;

	public static Connection getConnection() {
		String dbUrl = "jdbc:mysql://localhost:3306/postcard_db";
		Properties connectionProperties = new Properties();
		connectionProperties.put("user", "jm");
		connectionProperties.put("password", "password");
		loadDriver(); 
		try {
			connection = DriverManager.getConnection(dbUrl, connectionProperties);
		} catch (SQLException e) {
			log.error("Error on creating database connection: " + e);
		}

		return connection;
	}
	

	private static void loadDriver() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver").getConstructor().newInstance();
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException | ClassNotFoundException e) {
			log.error("Error on loading driver: " + e);
		}
		
	}

	public static void closeConnection(Connection connection) {
		try {
			connection.close();
		} catch (SQLException e) {
			log.error("Error on closing database connection: " + e);
		}
	}

}
