package ee.bcs.koolitus.postcards.dao;

import java.util.Date;
import java.util.Objects;

public class Transcript implements Comparable<Transcript> {
	private int transcriptId;
	private String transcriptText;
	private String userName;
	private Date datetime;
	private int cardId;

	public int getTranscriptId() {
		return transcriptId;
	}

	public void setTranscriptId(int transcriptId) {
		this.transcriptId = transcriptId;
	}

	public String getTranscriptText() {
		return transcriptText;
	}

	public void setTranscriptText(String transcriptText) {
		this.transcriptText = transcriptText;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getDatetime() {
		return datetime;
	}

	public void setDatetime(Date datetime) {
		this.datetime = datetime;
	}

	public int getCardId() {
		return cardId;
	}

	public void setCardId(int cardId) {
		this.cardId = cardId;
	}

	@Override
	public String toString() {
		return "CardTranscript[transcriptId= " + transcriptId + ", userName = " + userName + ", datetime " + datetime
				+ ", cardId " + cardId + ", transcriptText [ " + transcriptText + " ]" + " ]";
	}

	
	@Override
	public int compareTo(Transcript transcript) {
		if (this.equals(transcript)) {
			return 0;
		} else if (transcript.datetime.compareTo(this.datetime) != 0) {
			return transcript.datetime.compareTo(this.datetime) ;
		}

		return 0;
	}
/*
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;

		}
		if (obj == null) {
			return false;

		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final CardTranscript other = (CardTranscript) obj;
		boolean variablesEquals = this.transcriptId == other.transcriptId;
		return variablesEquals;

	}
*/
	@Override
	public int hashCode() {
		int hash = 7;

		hash = 31 * hash + Objects.hashCode(transcriptId);
		hash = 31 * hash + Objects.hashCode(userName);
		hash = 31 * hash + Objects.hashCode(datetime);
		hash = 31 * hash + Objects.hashCode(cardId);
		hash = 31 * hash + Objects.hashCode(transcriptId);

		return hash;

	}

}
