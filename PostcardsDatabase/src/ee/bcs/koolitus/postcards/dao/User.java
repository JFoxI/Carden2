package ee.bcs.koolitus.postcards.dao;

public class User {
	
	//private int userId;
	private String username;
	private String pwHash;
	
//	public int getId() {
//		return userId;
//	}
//	public void setId(int userId) {
//		this.userId = userId;
//	}
//	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPwHash() {
		return pwHash;
	}
	public void setPwHash(String pwHash) {
		this.pwHash = pwHash;
	}
}
