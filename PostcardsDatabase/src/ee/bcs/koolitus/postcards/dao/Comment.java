package ee.bcs.koolitus.postcards.dao;

import java.util.Date;
import java.util.Objects;

public class Comment implements Comparable<Comment> {
	private int commentId;
	private String commentText;
	private String userName;
	private Date datetime;
	private int cardId;

	public int getCommentId() {
		return commentId;
	}

	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getDatetime() {
		return datetime;
	}

	public void setDatetime(Date datetime) {
		this.datetime = datetime;
	}

	public int getCardId() {
		return cardId;
	}

	public void setCardId(int cardId) {
		this.cardId = cardId;
	}

	@Override
	public int compareTo(Comment comment) {
		if (this.equals(comment)) {
			return 0;

		} else if (comment.datetime.compareTo(this.datetime) != 0) {
			return comment.datetime.compareTo(this.datetime);
		}

		return 0;
	}

	
	@Override
	public int hashCode() {
		int hash = 3;
		hash = 31 * hash + Objects.hashCode(commentId);
		hash = 31 * hash + Objects.hashCode(commentText);
		hash = 31 * hash + Objects.hashCode(userName);
		hash = 31 * hash + Objects.hashCode(datetime);
		hash = 31 * hash + Objects.hashCode(cardId);

		return hash;
	}

	
	@Override
	public String toString() {
		return "CardComment[ commentId=" + commentId + ", trancriptText=" + commentText + ", userId=" + userName
				+ ", datetime=" + datetime + ", cardId=" + cardId + "]";
	}

}
