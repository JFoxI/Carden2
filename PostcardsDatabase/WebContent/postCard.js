var nomoreCardsURL="/PostcardsDatabase/Endmessage.png";
var currentCardID;
var radioButtonOne = document.getElementById("firstRadioButton");
var frontImageURLPrefix = "https://ajapaik.ee/media/uploads/DIGAR_";
var frontImageURLSufix = "_1.jpg";
var backImageURLPrefix = "https://ajapaik.ee/media/uploads/Digar_postkaartide_tagakyljed/DIGAR_";
var backImageURLSufix = "_2.jpg";


var currentUsername = document.cookie.split('=');
document.getElementById("userInAction").innerHTML = "Kasutaja: " + currentUsername[1];


function togglestuff() {	
	
	if (radioButtonOne.checked) {		
		$("#text_field").load("textfieldsUnreviewed.html");
	} else {
		$("#text_field").load("textfieldsReviewed.html");		
	}
}

function getFirstUnreviewedPostcardID() {
	
	togglestuff();

	$.ajax({
		url : "/PostcardsDatabase/rest/postcards",
		type : "GET",
		contentType : "text/plain; charset=utf-8",

		success : function(firstPostcardID) {
			var imageFrontContent = "<img src='" + frontImageURLPrefix + firstPostcardID + frontImageURLSufix + "'>";
			var imageBackContent = "<img src='" + backImageURLPrefix + firstPostcardID + backImageURLSufix + "'>";
			
			currentCardID = firstPostcardID;
			
			document.getElementById("front").innerHTML = imageFrontContent;
			document.getElementById("back").innerHTML = imageBackContent;
		},
		error : function(XMLHTTP, textStatus, errorThrown) {
			console.log("error getting first image");
			console.log(errorThrown.message);
		}

	});

}

function checkCookie() {
	var user = document.cookie;	
	    if (user != "") {
	    	getFirstUnreviewedPostcardID();
	    } else {
	    	$("body").load("index.html");
	    }
}

checkCookie();

function getNextUnreviewedPostcardID() {
	
	$.ajax({
		url : "/PostcardsDatabase/rest/postcards/" + currentCardID,
		type : "GET",
		contentType : "text/plain; charset=utf-8",

		success : function(nextPostcardID) {

			var imageFrontContent = "<img src='" + frontImageURLPrefix + nextPostcardID + frontImageURLSufix + "'>";
			var imageBackContent = "<img src='" + backImageURLPrefix + nextPostcardID + backImageURLSufix + "'>";
			
			currentCardID = nextPostcardID;
			
			document.getElementById("front").innerHTML = imageFrontContent;
			document.getElementById("back").innerHTML = imageBackContent;
		},
		error : function(XMLHTTP, textStatus, errorThrown) {
			console.log("error getting next image");
			console.log(errorThrown.message);
		}

	});

}

function getFirstReviewedPostcardID() {

	togglestuff();

	$.ajax({
			url : "/PostcardsDatabase/rest/postcards/reviewed",
			type : "GET",
			contentType : "text/plain; charset=utf-8",

			success : function(firstPostcardID) {

				var imageFrontContent = "<img src='" + frontImageURLPrefix + firstPostcardID + frontImageURLSufix + "'>";
				var imageBackContent = "<img src='" + backImageURLPrefix + firstPostcardID + backImageURLSufix + "'>";
				currentCardID = firstPostcardID;
				getAllComments(currentCardID);
				getAllTranscripts(currentCardID);
				document.getElementById("front").innerHTML = imageFrontContent;
				document.getElementById("back").innerHTML = imageBackContent;
			},
			error : function(XMLHTTP, textStatus, errorThrown) {
				console.log("error getting first image");
				console.log(errorThrown.message);
			}

		});

}

function getNextReviewedPostcardID() {

	urlstring = "/PostcardsDatabase/rest/postcards/reviewed/" + currentCardID;

	$.ajax({
		url : urlstring,
		type : "GET",
		contentType : "text/plain; charset=utf-8",

		success : function(nextPostcardID) {
			if (nextPostcardID==0){
				document.getElementById("front").innerHTML = "<img src='"+nomoreCardsURL + "'>";
				document.getElementById("back").innerHTML = "<img src='"+nomoreCardsURL + "'>";
				
			$("#text_field").load("textFieldsEmpty.html");
				
			}
			else {
			var imageFrontContent = "<img src='" + frontImageURLPrefix + nextPostcardID + frontImageURLSufix + "'>";
			var imageBackContent = "<img src='" + backImageURLPrefix + nextPostcardID + backImageURLSufix + "'>";
			
			currentCardID = nextPostcardID;
			getAllComments(currentCardID);
			getAllTranscripts(currentCardID);
			document.getElementById("front").innerHTML = imageFrontContent;
			document.getElementById("back").innerHTML = imageBackContent;
			}
		},
		error : function(XMLHTTP, textStatus, errorThrown) {
			console.log("error getting next image");
			console.log(errorThrown.message);
		}

	});

}

function saveNewTextAndComment() {
	
	
	var transcript = {
		"userName":  document.getElementById("userInAction").innerHTML.slice(10),
		"cardId" : currentCardID,
		"transcriptText" : document.getElementById("main_remarks").value
	};
	var comment = {
		"userName": document.getElementById("userInAction").innerHTML.slice(10),
		"cardId" : currentCardID,
		"commentText" : document.getElementById("extra_remarks").value
	};

	var jsonTranscriptString = JSON.stringify(transcript);
	var jsonCommentString = JSON.stringify(comment);

	
	$.ajax({
		url : "/PostcardsDatabase/rest/transcripts",
		type : "POST",
		data : jsonTranscriptString,
		contentType : "application/json; charset=utf-8",
		success : function() {
			$.ajax({
				url : "/PostcardsDatabase/rest/comments",
				type : "POST",
				data : jsonCommentString,
				contentType : "application/json; charset=utf-8",
				success : function() {
					if(radioButtonOne.checked) {
						updateReviewStatusAndGetNextUnreviewedPostcard()
					} else {
						getNextReviewedPostcardID();
					}
				},
				error : function(XMLHTTP, textStatus, errorThrown) {
					console.log("error on adding comments");
				}
			});
		},
		error : function(XMLHTTP, textStatus, errorThrown) {
			console.log("error on adding transcripts");
		}
	});

	document.getElementById("extra_remarks").value = null;
	document.getElementById("main_remarks").value = null;

}



function updateReviewStatusAndGetNextUnreviewedPostcard(){
		
	$.ajax({						
		  url: "/PostcardsDatabase/rest/postcards/" + currentCardID,
		  type: "PUT",
		  success: function() {					
					getNextUnreviewedPostcardID();			
			  },
		  error: function (XMLHTTP, textStatus, errorThrown){console.log("error on updating status"); }
	  });
}

function getAllComments(cardID) {
	
	$.ajax({
		url : "/PostcardsDatabase/rest/comments/" + cardID,
				type : "GET",
				success : function(obj) {
					var commentHistory = "";
					for (var i = 0; i < obj.length ; i++) {
						if (obj[i].commentText.trim() != 0) {
							commentHistory += obj[i].datetime.split("T")[0] + "\t Kasutaja: " + obj[i].userName + "\n" + obj[i].commentText + "\n";
						}						
					}
					document.getElementById("extra_remarks_default").innerHTML = commentHistory;
				},
				error : function(XMLHTTP, textStatus, errorThrown) {
					console.log("error on getting all comments");
				}
			});
}

function getAllTranscripts(cardID) {
	
	$.ajax({
		url : "/PostcardsDatabase/rest/transcripts/" + cardID,
		type : "GET",
		success : function(obj) {			
			var transcriptHistory = "";
			for (var i = 0; i < obj.length ; i++) {
				if (obj[i].transcriptText.trim() != 0) {
					transcriptHistory += obj[i].datetime.split("T")[0] + "\t Kasutaja: " + obj[i].userName + "\n" + obj[i].transcriptText + "\n";
				}
			}
			
			document.getElementById("main_remarks_default").innerHTML = transcriptHistory;
		},
		error : function(XMLHTTP, textStatus, errorThrown) {
			console.log("error on getting all comments");
		}
	});
}
