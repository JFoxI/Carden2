function checkIfPasswordFieldsAreSame(){
	
	var PasswordFirstField = document.getElementById("userPassword").value;
	var controlPasswordSecondField = document.getElementById("newUserPasswordAgain").value;
	var UsernameField = document.getElementById("userName").value;
	var regex = new RegExp('[^a-zA-ZõäöüÕÄÖÜžšŽŠ 0-9]');

	if(PasswordFirstField.trim() == "" || UsernameField.trim() == ""){
		alert("Väljad ei tohi olla tühjad");
	} else if(!regex.test(UsernameField)) {
		if (PasswordFirstField === controlPasswordSecondField) {			
			checkUsername();
		}
		else{
			alert("Paroolid ei ole samad, kontrolli väljasid");
		}
	} else {
		alert("Lubatud on vaid eesti tähestik ja/või numbrid");
		}		
}

function checkUsername(){
	var jsonUser = {
			"username" : document.getElementById("userName").value
	};
	
	var jsonUserString = JSON.stringify(jsonUser);
	
	$.ajax({
		url: "/PostcardsDatabase/rest/users/checkUser",
		type: "POST",
		data: jsonUserString,
		contentType: "application/json; charset=utf-8",
		success: function(result){
			if(result == "false"){
				userCreation();
			} else {
				alert("Selline kasutaja on juba olemas")
				window.open("/PostcardsDatabase/newUser.html", "_self");
			}			
		},
		error: function (XMLHTTP, textStatus, errorThrown) {		
			console.log("error on checking username");
		}	
	});
}

function userCreation(){
	
	var jsonUser = {			
			"username" : document.getElementById("userName").value,
			"pwHash" : document.getElementById("userPassword").value
	};
	
	var jsonUserString = JSON.stringify(jsonUser);
	
	$.ajax({
		url: "/PostcardsDatabase/rest/users/newUser", 
		type: "POST",
		data: jsonUserString,
		contentType: "application/json; charset=utf-8",
		success: function(){
			alert("Õnnestus kasutaja loomine");
			userLogin();
		},
		error: function (XMLHTTP, textStatus, errorThrown) {		
			console.log("error on checking username and password");
		}		
	});
}

function userLogin(){
	
	var jsonData = {
			"username" : document.getElementById("userName").value,
			"pwHash" : document.getElementById("userPassword").value
	};
	
	var jsonDataString = JSON.stringify(jsonData);
	
	$.ajax({
		url: "/PostcardsDatabase/rest/users", 
		type: "POST",
		data: jsonDataString,
		contentType: "application/json; charset=utf-8",
		success: function(userResponse){			
			if(userResponse == "true"){
				function openWorkbench(){
					var name = document.getElementById("userName").value;
					document.cookie = "username=" + name + ";path=/";
					var url = "/PostcardsDatabase/postCard.html";
					window.open(url, "_self");
					}			
				openWorkbench();		
			} else {
				function openWorkbench(){
					window.open("/PostcardsDatabase/index.html", "_self"); 
					}
				openWorkbench();
				alert("Kontrolli sisestatud kasutajanime ja/või parooli!");
			}	
		},
		error: function (XMLHTTP, textStatus, errorThrown) {		
			console.log("error on checking username and password");
		}		
	});
}


function userLogout(){
	document.cookie = "username=" + "; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/";
}

